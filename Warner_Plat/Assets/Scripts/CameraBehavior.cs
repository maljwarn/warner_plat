﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour {
	public GameObject player;
	private Vector3 offset;
	public float lifeTime;
	public GameObject controls;
	public GUIText healthText;
	public GameObject boss;
	// Use this for initialization
	void Start () {
		healthText.enabled = false;
		Destroy (controls, lifeTime);
		if (player != null) {
		offset = transform.position - player.transform.position;
		}
	}
	void Update(){
			
		UpdateHealth ();

	}
	void LateUpdate () {
		if (player != null) {
			transform.position = player.transform.position + offset;
		}
	}

	void UpdateHealth (){
		healthText.text = "Boss Health: " + GameObject.FindWithTag("Boss").GetComponent<BossBehavior>().health;
	}
}



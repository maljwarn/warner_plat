﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
	[HideInInspector] public bool facingRight = true;
	public float maxSpeed = 5f;
	public float moveForce = 365f;
	private Rigidbody2D rb2d;
	bool CanFlip = true;
	public GameObject coco;
	public Transform[] cocoSpawn;
	public float health;
	private bool drop;
	private AudioSource audioSource;
			
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		drop = false;
		audioSource = GetComponent<AudioSource> ();
	}

	void Update () {
		float h = Input.GetAxis("Horizontal");
		if (health <= 0) {
			Destroy (gameObject);
			drop = true;
		}
		if (drop) {
			Drop ();
		}
	}

	void FixedUpdate()
	{
		float h = -1;
		if (facingRight == true)
			h = 1;
		
		if (h * rb2d.velocity.x < maxSpeed)
			rb2d.AddForce(Vector2.right * h * moveForce);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Bullet")) {
			health -= 1;
			Destroy (other.gameObject);
		}
		if (other.gameObject.CompareTag ("End")) {
			Flip ();
		}			
	}
	void OnCollisionEnter2D (Collision2D coll){		
		
		if (coll.gameObject.CompareTag ("Player")) 
		{
			audioSource.Play ();
		}
	}

	void Flip()
	{
		if (CanFlip == true) {
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
			CanFlip = false;
			Invoke("RefreshFlip", 1f);
			rb2d.velocity = Vector3.zero;
		}
	}

	void RefreshFlip ()
	{
		CanFlip = true;
	}

	void Drop(){
		for (int i = 0; i < cocoSpawn.Length; i++)
		{
			int cocoS = Random.Range (0, 2);
			if (cocoS > 0) {
				Instantiate (coco, cocoSpawn [i].position, cocoSpawn [i].rotation);
			}
		}
		drop = false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEmeny : MonoBehaviour {

	public Transform[] enemySpawns;
	public GameObject enemy;

	void Start () {
		Spawn();
	}

	void Spawn()
	{
		for (int i = 0; i < enemySpawns.Length; i++)
		{
			int enemyFlip = Random.Range (0, 2);
			if (enemyFlip > 0)
				Instantiate(enemy, enemySpawns[i].position, Quaternion.identity);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickUp : MonoBehaviour {
	public GameObject gun;
	public GameObject shot;
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Gun")) {
			Destroy(other.gameObject);
			gun.SetActive (true);
			shot.SetActive (true);
		}	
	}

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour {

	private Rigidbody2D rb;
	public float speed;
	[HideInInspector] public bool shootRight;
	public float lifeTime;

	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		if (GameObject.FindWithTag ("Boss").GetComponent<BossBehavior>().facingRight == true) {
			rb.velocity = transform.right * speed;
			Destroy (gameObject, lifeTime);
		} 
		if (GameObject.FindWithTag ("Boss").GetComponent<BossBehavior>().facingRight == false){
			rb.velocity = transform.right * -speed;
			Destroy (gameObject, lifeTime);
		}
			
	}

}
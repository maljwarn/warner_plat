﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : MonoBehaviour {

	public Transform bossSpawn;
	public GameObject boss;
	public Transform cookieSpawn;
	public GameObject cookie;
	private bool canSpawn = true;

	void Start () {
		Spawn();
	}

	void Spawn()
	{
		if (canSpawn) {
			Instantiate(boss, bossSpawn.position, Quaternion.identity);
			Instantiate(cookie, cookieSpawn.position, Quaternion.identity);
			canSpawn = false;
		} 

	}
}

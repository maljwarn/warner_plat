﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupCoin : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D coll){	
		if (gameObject != null) {
			if (coll.gameObject.CompareTag ("Player"))
			{
				Destroy(gameObject);
				GameObject.FindGameObjectWithTag ("Player").GetComponent<SimplePlatformerController> ().ammo += 1;
			}
		}
	

}
}

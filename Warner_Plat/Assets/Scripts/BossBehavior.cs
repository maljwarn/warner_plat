﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehavior : MonoBehaviour {
	public float fireRate;
	private float nextFire;
	public Transform shotSpawn;
	public GameObject shot;
	private AudioSource audioSource;
	public GameObject player;
	public float health;
	public bool facingRight;

	void Start () {
		audioSource = GetComponent<AudioSource> ();
		myTransform = transform;
		facingRight = false;
	}

	void Update () {
		if (health <= 0) {
			Destroy (gameObject);
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("MainCamera")) {
			other.GetComponent<CameraBehavior> ().healthText.enabled = true;
			player = GameObject.FindGameObjectWithTag ("Player");

			if (Time.time > nextFire) {
				nextFire = Time.time + fireRate;
				Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
				audioSource.Play ();

				if (transform.position.x + .2 < player.transform.position.x) {
					// face right
					facingRight = true;
					transform.localScale = new Vector3 (2f,2f,2f);

				} else if (transform.position.x - .2 > player.transform.position.x) {
					// face left
					facingRight = false;
					transform.localScale = new Vector3 (-2f,2f,2f);
				}
			}
		}
}
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Bullet")) {
			health -= 1;
			Destroy (other.gameObject);
		}

	}
}


  
﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatformerController : MonoBehaviour {

	[HideInInspector] public bool facingRight = true;
	[HideInInspector] public bool jump = false;
	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	public Transform groundCheck;
	public float fireRate;
	private float nextFire;
	public Transform shotSpawn;
	public GameObject shot;
	public float ammo = 5f;
	public GameObject gun;
	public float health = 10f;
	private bool grounded = false;
	private Rigidbody2D rb2d;
	public float knockBack;
	public float knockBackLenght;
	public float knockBackCount;
	public bool knockFromRight;
	public GUIText healthText;
	public GUIText ammoText;
	public GameObject lose;
	public GameObject win;
	private bool restart;
	public float knockbackForce;
	public float knockbackTime;
	private float knockbackCounter;
	private AudioSource audioSource;

	void Awake () 
	{
		rb2d = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource> ();
	}
	void Start()
	{
		restart = false;
	}
	void Update () 
	{
		if (health >= 0) {
			UpdateHealth ();
		}
		UpdateAmmo ();
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		if (Input.GetButtonDown("Jump") && grounded)
		{
			jump = true;
		}
		if (gun.activeInHierarchy) {
			if (Input.GetButton ("Fire1") && Time.time > nextFire && ammo > 0) {
				nextFire = Time.time + fireRate;
				Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
				audioSource.Play ();
				ammo -= 1;
			}
		}
		if (health <= 0) {
			lose.SetActive (true);
			restart = true;
		}
		if (restart) 
		{ 
			if (Input.GetButton ("Submit")) 
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
	}
	void OnCollisionEnter2D (Collision2D coll){		
		
		if (coll.gameObject.CompareTag ("Enemy") || coll.gameObject.CompareTag("Boss")) {
			health -= 1;
			KnockBack (rb2d.velocity);
		}	
		if (coll.gameObject.CompareTag ("CoCo"))
		{
			Destroy(coll.gameObject);
			ammo += 1;
		}
	}
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Cookie")) {
			win.SetActive (true);
			restart = true;
		}	
		if (other.gameObject.CompareTag ("Enemy")) {
			health -= 1;
		}
	} 
	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");
		if (knockBackCount <= 0) {
			if (restart == false) {
				if (h * rb2d.velocity.x < maxSpeed)
					rb2d.AddForce (Vector2.right * h * moveForce);
				if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
					rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
				if (h > 0 && !facingRight) {
					Flip ();
				} else if (h < 0 && facingRight)
					Flip ();
				if (jump){
					rb2d.AddForce (new Vector2 (0f, jumpForce));
					jump = false;
				}
			}
		} else
			knockbackCounter -= Time.deltaTime;
	}
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	void UpdateHealth (){
		healthText.text = "Health: " + health;
	}	
	void UpdateAmmo (){
		ammoText.text = "Ammo: " + ammo;
	}
	void KnockBack(Vector2 direction){
		direction = new Vector3 (1f, 1f, 1f);
		knockbackCounter = knockbackTime;
		rb2d.velocity = direction * knockbackForce;
	}
}

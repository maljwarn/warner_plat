﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire : MonoBehaviour {

	public GameObject shot;
	public Transform[] shotSpawn;
	public float fireRate;
	public float delay;

	void Start () {
		InvokeRepeating ("Fire", delay, fireRate);
	}

	void Fire (){
		for (int i = 0; i < shotSpawn.Length; i++)
		{
			int shotS = Random.Range (0, 2);
			if (shotS > 0) {
				Instantiate (shot, shotSpawn [i].position, shotSpawn [i].rotation);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementShot : MonoBehaviour {

	private Rigidbody2D rb;
	public float speed;
	[HideInInspector] public bool shootRight;
	public float lifeTime;

	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		if (GameObject.Find ("hero").GetComponent<SimplePlatformerController> ().facingRight == true) {
			rb.velocity = transform.right * speed;
			Destroy (gameObject, lifeTime);
		} 
		if (GameObject.Find ("hero").GetComponent<SimplePlatformerController> ().facingRight == false){
			rb.velocity = transform.right * -speed;
			Destroy (gameObject, lifeTime);
		}
	}

	}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlatforms : MonoBehaviour {

	public int maxPlatforms = 20;
	public GameObject platform;
	public float horizontalMin = 10f;
	public float horizontalMax = 14f;
	public float verticalMin = -6f;
	public float verticalMax = 6;
	public float bosshorizontalMin = 10f;
	public float bosshorizontalMax = 14f;
	public float bossverticalMin = -6f;
	public float bossverticalMax = 6;
	public GameObject bossPlat;

	public Vector2 originPosition;


	void Start () {

		originPosition = transform.position;
		Spawn ();

	}
	void Update(){
		if (maxPlatforms == 10) {
			Vector2 randomPosition = originPosition + new Vector2 (Random.Range(bosshorizontalMin, bosshorizontalMax), Random.Range (bossverticalMin, bossverticalMax));
			Instantiate(bossPlat, randomPosition, Quaternion.identity);
			maxPlatforms = 0;
		}
	}

	void Spawn()
	{
		for (int i = 0; i < maxPlatforms; i++)
		{
			Vector2 randomPosition = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range (verticalMin, verticalMax));
			Instantiate(platform, randomPosition, Quaternion.identity);
			originPosition = randomPosition;
		}
	}
}
